# language: pt

Funcionalidade: Lista de Tarefas

    Como usuário, preciso inserir uma tarefa na minha
    lista de tarefas, para saber quais compromissos
    preciso cumprir.

    Cenario: Não deve aceitar uma entrada vazia no cadastro de tarefas
        Dado que o usuário não cadastrar a tarefa
        Quando tentar cadastrar a tarefa sem descrição
        Então deve mostrar uma mensagem de erro
