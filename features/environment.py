from selenium import webdriver

from django.core import management


def before_all(context):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')
    chrome_options.add_argument('disable-gpu')
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_argument('window-size=1920x1080')
    context.browser = webdriver.Chrome(options=chrome_options)

    context.base_url = "http://localhost:8000/"


def before_scenario(context, scenario):
    # Reset the database before each scenario
    # This means we can create, delete and edit objects within an
    # individual scenerio without these changes affecting our
    # other scenarios
    management.call_command('flush', verbosity=0, interactive=False)


def after_all(context):
    context.browser.quit()
