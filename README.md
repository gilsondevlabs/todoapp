# Django Todo App

[![pipeline status](https://gitlab.com/gilsondev/todoapp/badges/master/pipeline.svg)](https://gitlab.com/gilsondev/todoapp/commits/master)
[![coverage report](https://gitlab.com/gilsondev/todoapp/badges/master/coverage.svg)](https://gitlab.com/gilsondev/todoapp/commits/master)

Projeto de exemplo, para apresentar um sistema de lista de tarefas que foi construído em cima de boas práticas de engenharia, como testes automatizados (TDD e BDD).

**Staging**: [todoapp-staging.herokuapp.com](https://todoapp-staging.herokuapp.com)
**Production**: [todoapp-prod.herokuapp.com](https://todoapp-prod.herokuapp.com/)

## Estória de Usuário

Abaixo segue a estória do usuário para a funcionalidade criada:

```
Estória: Lista de Tarefas

Como usuário, preciso inserir uma item na minha lista de tarefas
para saber quais compromissos preciso cumprir.

Critérios de aceite:

 - Deve aceitar o cadastro de uma tarefa
 - Não deve aceitar uma entrada vazia na lista de tarefas
```

## Requisitos mínimos

* Python 3.6
* Docker
* Docker Compose
* [Chromedriver](http://chromedriver.chromium.org/home) (para execução dos testes em modo headless)

## Instalação

1.  Faça o checkout do projeto e crie o ambiente virtual

```shell
$ git clone ...
$ cd todoapp
$ python -m venv .venv
```

2.  Ative o ambiente virtual e instale as dependências

```shell
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

3.  Copie o arquivo `contrib/env-sample` e altere as propriedades desejadas.

4.  Levante o container do banco de dados

```shell
$ docker-compose up -d db
```

### Executando aplicação com Docker

Para desenvolvimento local, você pode também levantar um container da aplicação. Com o docker-compose, a cada mudança, será aplicada ao container.
Dessa forma execute o comando abaixo, ao invés do item 4 acima:

```shell
$ docker-compose up -d
```

Isso irá levantar o container do banco, como gerar uma imagem do sistema e criar o container do mesmo.

## Testes

Os testes foram feitos, tanto unitários como funcionais. Dessa forma, para executálos é só seguir os comandos abaixo:

* Testes unitários

```shell
$ tox
```

* Testes funcionais (BDD)

```shell
$ python manage.py behave
```

## Versão do Projeto

Nós usamos o `bumpversion` para automatizar a mudança de versão do sistema em que executa o seguinte procedimento:

* Altera a versão no arquivo `todoapp/__init__.py`
* Altera a versão na propriedade `release` do Raven (Sentry)

Dessa forma, siga os passos abaixo:

```shell
$ bumpversion /patch/minor/major
$ git push --tags origin master
```

## Deploy

O projeto está preparado para efetuar o deploy no Heroku. Para isso é necessário fazer algumas preparações no ambiente:

```shell
heroku config:set SECRET_KEY=`python contrib/secret_gen.py`
heroku config:set DEBUG=False
heroku config:set ALLOWED_HOSTS=.herokuapp.com

# Instale o banco de dados com o plano desejado
heroku addons:create heroku-postgresql:hobby-dev

# Instale o monitoramento de erros com Sentry
heroku addons:create sentry:f1

# Instale e configure o email com sendgrid
heroku addons:create sendgrid:starter
heroku config:set EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend
heroku config:set EMAIL_HOST='smtp.sendgrid.net'
heroku config:set EMAIL_PORT=587
heroku config:set EMAIL_USE_TLS=True
heroku config:set EMAIL_HOST_USER=`heroku config:get SENDGRID_USERNAME`
heroku config:set EMAIL_HOST_PASSWORD=`heroku config:get SENDGRID_PASSWORD`
```
